quizRaces = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "What’s racing?",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about race conditions.",
            "a": [
                {"option": "Race conditions can arise between any pair of threads.", "correct": false},
                {"option": "If two threads access shared resources and at least one of them modifies state, race conditions can arise.", "correct": true},
                {"option": "“Mutex” is just another word for “lock”.", "correct": true},
                {"option": "“Semaphores” can be used to prevent race conditions.", "correct": true},
                {"option": "When multiple threads share resources, some programming discipline is required to prevent race conditions.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: Most statements are correct.)</span> Please check out <a href=\"Operating-Systems-MX.html#slide-race-condition\">earlier slides</a>.</p>" // no comma here
        }
    ]
};
