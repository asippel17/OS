quizThreadTerminology = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Threads, concurrency, and preemption",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about thread terminology",
            "a": [
                {"option": "Running programs are managed as threads by the OS.", "correct": false},
                {"option": "Processes and threads are OS management units.", "correct": true},
                {"option": "Each thread may contain one or more threads.", "correct": false},
                {"option": "Concurrency can only arise on multi-core systems.", "correct": false},
                {"option": "Scheduling may lead to interleaved executions of multiple threads.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 2 statements are correct.)</span> Maybe <a href=\"Operating-Systems-JiTT.html#slide-help\">ask for help</a>?</p>" // no comma here
        },
        {
            "q": "Select correct statements about preemption",
            "a": [
                {"option": "Concurrency can cause preemption.", "correct": false},
                {"option": "Preempted threads are garbage-collected by the OS.", "correct": false},
                {"option": "In response to an interrupt, a thread may be preempted.", "correct": true},
                {"option": "Preemption hinders effective caching.", "correct": true},
                {"option": "Management information on stacks can used to resume preempted threads later on.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span> Maybe check out <a href=\"Operating-Systems-Threads.html#slide-preemption\">earlier slides</a>?</p>" // no comma here
        }
    ]
};
