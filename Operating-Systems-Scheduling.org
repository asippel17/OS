# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: "config-course.org"

#+TITLE: OS04: Scheduling
#+SUBTITLE: Based on Chapter 3 of cite:Hai17

#+MACRO: jittquiz [[https://sso.uni-muenster.de/LearnWeb/learnweb2/mod/quiz/view.php?id=1212243][Learnweb]]

* Introduction

# This is the fifth presentation for this course.
#+CALL: generate-plan(number=5)

** Previously on OS …
   - What is [[file:Operating-Systems-Introduction.org::#multitasking][multitasking]]?
   - What are [[file:Operating-Systems-Interrupts.org::#blocking][blocking]] system calls?
   - What [[file:Operating-Systems-Threads.org::#classification][types of threads]] exist?
   - What is [[file:Operating-Systems-Threads.org::#preemption][preemption]]?

*** CPU Scheduling
    :PROPERTIES:
    :CUSTOM_ID: drawing-scheduling
    :END:
    #+INDEX: Scheduling!Drawing (Scheduling)
    {{{revealimg("./figures/external-non-free/jvns.ca/scheduling.meta",t,"50vh")}}}

** Today’s Core Questions
   - How does the OS manage the shared resource CPU?  What goals are pursued?
   - How does the OS distinguish threads that could run on the CPU
     from those that cannot (i.e., that are blocked)?
   - How does the OS schedule threads for execution?

** Learning Objectives
   - Explain thread concept (continued)
     - Including states and priorities
   - Explain scheduling mechanisms and their goals
   - Apply scheduling algorithms
     - FCFS, Round Robin

** Retrieval Practice
   - Before you continue, answer the following; ideally, without
     outside help.
     - What is a process, what a thread?
     - What does concurrency mean?
       - How does it arise?
     - What is preemption?

*** Thread Terminology
#+REVEAL_HTML: <script data-quiz="quizThreadTerminology" src="./quizzes/thread-terminology.js"></script>

#+TOC: headlines 1

* Scheduling
** CPU Scheduling
   :PROPERTIES:
   :CUSTOM_ID: cpu-scheduling
   :END:
   #+INDEX: Scheduling!Definition (Scheduling)
   #+INDEX: Scheduling!Preemption (Scheduling)
   #+INDEX: Preemption (Scheduling)
   - With multitasking, lots of threads *share resources*
     - Focus here: CPU
   - *Scheduling* (planning) and *dispatching* (allocation) of CPU via OS
     #+ATTR_REVEAL: :frag (appear)
     - *Non-preemptive*
       - Thread on CPU until [[file:Operating-Systems-Threads.org::#yielding][yield]],
         termination, or [[file:Operating-Systems-Interrupts.org::#blocking][blocking]]
     - *Preemptive*
       - Typical case for desktop OSs
         1. Among all threads, schedule and dispatch one, say T_0
         2. Allow T_0 to execute on CPU for some time, then preempt it
         3. Repeat step (1)
   #+ATTR_REVEAL: :frag appear
   - (Similar decisions take place in industrial production, which you may know from
     operations management)

** Scheduling Goals
   - Fairness
   - Response time
   - Throughput
   - Efficient resource usage

   Note: Above goals have *trade-offs* (discussed subsequently)

* Thread States
  :PROPERTIES:
  :CUSTOM_ID: sec-thread-states
  :END:

** Reasons to Distinguish States
   - Recall: Some threads may be waiting (synonym: be blocked)
     - E.g., wait for I/O operation to finish or ~sleep~ system call
       (recall [[file:Operating-Systems-Threads.org::#Simpler2Threads][Simpler2Threads]])
       - More generally, threads may perform [[file:Operating-Systems-Interrupts.org::#blocking][blocking]] system calls
     - [[file:Operating-Systems-Interrupts.org::#polling-pro-con][Busy waiting]]
       would be a waste of CPU resources
       - If other threads could run on CPU
   #+ATTR_REVEAL: :frag appear
   - OS distinguishes thread states to tell threads apart that might
     perform useful computations (runnable) on the CPU from those that
     do not (blocked/waiting)
     - Scheduler does not need to consider waiting threads
     - Scheduler considers runnable threads, selects one, dispatches
       that for execution on CPU (which is then running)

** OS Thread States
   :PROPERTIES:
   :CUSTOM_ID: thread-states
   :END:
   #+INDEX: Thread states!Definition (Scheduling)
   #+INDEX: Running thread (Scheduling)
   #+INDEX: Blocked thread (Scheduling)
   #+INDEX: Waiting thread (Scheduling)
   - Different OSs distinguish different sets of states; typically:
     - *Running*: Thread(s) currently executing on CPU (cores)
     - *Runnable*: Threads ready to perform computations
     - *Waiting* or *blocked*: Threads waiting for some event to occur
   #+ATTR_REVEAL: :frag appear
   - OS manages states via [[https://en.wikipedia.org/wiki/Queue_(abstract_data_type)][queues]]
     (with suitable data structures)
     - *Run queue(s)*: Potentially per CPU core
       - Containing runnable threads, input for scheduler
     - *Wait queue(s)*: Potentially per event (type)
       - Containing waiting threads
         - OS inserts running thread here upon blocking system call
         - OS moves thread from here to run queue when event occurs

** Thread State Transitions
   :PROPERTIES:
   :CUSTOM_ID: state-transitions
   :reveal_extra_attr: data-audio-src="./audio/4-thread-states.ogg"
   :END:
   #+INDEX: Thread states!Transitions (Scheduling)
   {{{revealimg("./figures/OS/hail_f0303.pdf.meta",t,"50vh")}}}
#+BEGIN_NOTES
This diagram shows typical state transitions caused by actions of
threads, decisions of the OS, and external I/O events.  State changes
are always managed by the OS.

Newly created threads, such as the ones you created in Java, are Runnable.
When the CPU is idle, the OS’ scheduler executes a selection algorithm
among the Runnable threads and dispatches one to run on the CPU.  When
that thread yields or is preempted, the OS remembers that thread as
Runnable.

If the thread invokes a blocking system call, the OS changes its
state to Waiting.  Once the event for which the thread waits has
happened (e.g., a key pressed or some data has been transferred from
disk to RAM), the OS changes the state from Waiting to Runnable.
At some later point in time, that thread may be selected by the
scheduler to run on the CPU again.

In addition, an outgoing arc Termination is shown from state Running,
which indicates that a thread has completed its computations (e.g.,
the main function in Java ends).  Actually, threads may also be
terminated in states Runnable and Waiting, which is not shown here,
but which can happen if a thread is killed (e.g., you end a program or
shut down the machine).
#+END_NOTES

** Ticket to Exam - Task 1
   :PROPERTIES:
   :CUSTOM_ID: ticket-3-1
   :reveal_data_state: jitt no-toc-progress
   :END:

   Submit your solution to the following task in {{{learnweb}}}.

   Construct an example of your own choice where a thread changes states,
   involving all three states *running*, *runnable*, and *blocked*.
   Why and when do the state changes occur?
   At what points in time is the OS involved how?


* Scheduling Goals

** Goal Overview
  - Performance
    - *Throughput*
      - Number of completed threads (computations, jobs) per time unit
      - More important for service providers than users
    - *Response time*
      - Time from thread start or interaction to useful reaction
  #+ATTR_REVEAL: :frag appear
  - User control
    - *Urgency*
    - *Importance*
    - *Resource allocation*

*** Throughput
    - Avoid threads on CPU that are waiting
    - Context switching necessary
    - Recall: [[file:Operating-Systems-Threads.org::#multitasking-overhead][Overhead of context switching]] reduces throughput
      - Minor source: Saving state
      - Major sources: Cache pollution, [[https://en.wikipedia.org/wiki/Cache_coherence][cache coherence protocols]]

*** Response Time
    - Frequent context switches may help for small response time
      - However, their overhead hurts throughput
    - Responding quickly to one thread may slow down another one
      - Use urgency or importance to prioritize as explained next

*** Urgency vs Importance
    - Urgent: When due?
      - Upcoming *deadline*; more urgent if sooner
    - Important: How much at stake?
      - Tasks associated with *costs*
      - E.g., distinguish timely submissions of JiTT task vs seminar paper

*** Resource Allocation
    - What fraction of resources for what purpose?
    #+ATTR_REVEAL: :frag appear
    - *Proportional share scheduling*
      - E.g., multi-user machine: Different users obtain same share
        of CPU time every second
        - Unless one pays more: Larger share for that user
    #+ATTR_REVEAL: :frag appear
    - *Group scheduling*: Assign threads to groups, each of which
      receives its proportional share
      @@html:<br>@@ → Linux scheduler [[#cfs][later on]]

** Priorities in Practice
   :PROPERTIES:
   :CUSTOM_ID: priority-practice
   :END:
   #+INDEX: Priority (Scheduling)
   #+INDEX: Thread!Priority (Scheduling)
   - Different OSs (and execution environments such as Java) provide
     different means to express priority
     - E.g., numerical priority, so-called niceness value, deadline, …
     - Upon thread creation, its priority can be specified (by the
       programmer, with default value)
       - Priority recorded in [[file:Operating-Systems-Threads.org::#tcb][TCB]]
       - Sometimes, administrator privileges are necessary for “high”
         priorities
       - Also, OS tools may allow to change priorities at runtime
   - Rarely expressive enough to distinguish above cases entirely

* Scheduling Mechanisms
** Three Families of Schedulers
#+BEGIN_leftcol
- [[#fixed-priority][Fixed thread priorities]]
- [[#dynamic-priority][Dynamically adjusted thread priorities]]
- [[#proportional-share][Controlling proportional shares of processing time]]
#+END_leftcol
#+BEGIN_rightcol
{{{reveallicense("./figures/OS/4-scheduling-mechanisms.meta","50vh",nil,none)}}}
#+END_rightcol

*** Notes on Scheduling
    - For scheduling with pen and paper, you need to know
      *arrival times* and *service times* for threads
      - Arrival time: Point in time when thread created
      - Service time: CPU time necessary to complete thread
        - (For simplicity, blocking I/O is not considered; otherwise,
          you would also need to know frequency and duration of I/O
          operations)
    #+ATTR_REVEAL: :frag appear
    - OS does not know either ahead of time
      - OS creates threads (so, knowledge of arrival time is not an
        issue) and inserts them into necessary data structures during
        normal operation
      - When threads terminate, OS again participates
        - Thus, OS can compute service time after the fact
        - (Some scheduling algorithms require service time for
          scheduling decisions; then threads need to declare that upon
          start.  Not considered here.)

** Fixed-Priority Scheduling
   :PROPERTIES:
   :CUSTOM_ID: fixed-priority
   :END:
   #+INDEX: Scheduling!Fixed-priority (Scheduling)
   #+INDEX: Fixed-priority scheduling (Scheduling)
   - Use *fixed*, numerical *priority* per thread
     - Threads with higher priority preferred over others
       - Smaller or higher numbers may indicate higher priority:
         OS dependent
   #+ATTR_REVEAL: :frag appear
   - Implementation alternatives
     - Single queue ordered by priority
     - Or one queue per priority
       - OS schedules threads from highest-priority non-empty queue
   #+ATTR_REVEAL: :frag appear
   - Scheduling whenever CPU idle or some thread becomes runnable
     - Dispatch thread of highest priority
       - In case of ties: Run one until end ([[#fifo][FIFO]]) or
         serve all [[#roundrobin][Round Robin]]

*** FIFO/FCFS Scheduling
    :PROPERTIES:
    :CUSTOM_ID: fifo
    :END:
    #+INDEX: Scheduling!FIFO (Scheduling)
    #+INDEX: FIFO scheduling (Scheduling)
    - FIFO = First in, first out
      - (= FCFS = first come, first served)
      - Think of queue in supermarket
    - Non-preemptive strategy: Run first thread until completed (or blocked)
      - For threads of equal priority

*** Round Robin Scheduling
    :PROPERTIES:
    :CUSTOM_ID: roundrobin
    :END:
    #+INDEX: Scheduling!Round robin (Scheduling)
    #+INDEX: Round robin scheduling (Scheduling)
    #+INDEX: Time slice (Scheduling)
    #+INDEX: Quantum (Scheduling)
    #+INDEX: Queue (Scheduling)
    - Key ingredients
      - *Time slice* (quantum, q)
        - Timer with interrupt, e.g., every 30ms
      - *Queue(s)* for runnable threads
        - Newly created thread inserted at end
      #+ATTR_REVEAL: :frag appear
      - Scheduling when (1) timer interrupt triggered or (2) thread ends
        or is blocked
        #+ATTR_REVEAL: :frag (appear)
        1. Timer interrupt: *Preempt* running thread
           - Move previously running thread to end of runnable queue (for
             its priority)
           - Dispatch thread at head of queue (for highest priority) to CPU
             - With new timer for full time slice
        2. Thread ends or is blocked
           - Cancel its timer, dispatch thread at head of queue (for full
             time slice)
    #+ATTR_REVEAL: :frag appear
    - Video tutorial in {{{learnweb}}}

** Dynamic-Priority Scheduling
   :PROPERTIES:
   :CUSTOM_ID: dynamic-priority
   :END:
   #+INDEX: Scheduling!Dynamic-priority (Scheduling)
   #+INDEX: Dynamic-priority scheduling (Scheduling)
   - With dynamic strategies, OS can adjust thread priorities during execution
   - Sample strategies
     - *Earliest deadline first*
       - For tasks with deadlines — discussed in cite:Hai17, not part of
         learning objectives
     - *Decay Usage Scheduling*

*** Decay Usage Scheduling
    :PROPERTIES:
    :CUSTOM_ID: decay-usage
    :END:
    #+INDEX: Scheduling!Decay usage (Scheduling)
    #+INDEX: Decay usage scheduling (Scheduling)
    - General intuition: I/O bound threads are at unfair disadvantage.
      (Why?)
      #+ATTR_REVEAL: :frag appear
      - *Decrease* priority of threads in *running state*
      - *Increase* priority of threads in *waiting state*
        - Allows quick reaction to I/O completion (e.g, user interaction)
    #+ATTR_REVEAL: :frag appear
    - Technically, threads have a *base priority* that is adjusted by OS
      - Use [[#roundrobin][Round Robin scheduling]] after priority adjustments
    - OS may manage one queue of threads per priority
      - Threads move between queues when priorities change
        - Falls under more general pattern of [[https://en.wikipedia.org/wiki/Multilevel_feedback_queue][multilevel feedback queue]]
          schedulers

*** Decay Usage Scheduling in Mac OS X
    - OS keeps track of CPU *usage*
      - Usage increases linearly for time spent on CPU
        - Usage recorded when thread leaves CPU (yield or preempt)
      - Usage *decays* exponentially while thread is waiting
    - Priority adjusted downward based on CPU usage
      - Higher adjustments for threads with higher usage
        - Those threads’ priorities will be lower than others

*** Variant in MS Windows
    - Increase of priority when thread leaves waiting state
      - Priority *boosting*
      - Amount of boost depends on wait reason
        - More for interactive I/O (keyboard, mouse) then other types
    - After boost, priority decreases linearly with increasing CPU
      usage

** Proportional-Share Scheduling
   :PROPERTIES:
   :CUSTOM_ID: proportional-share
   :END:
   #+INDEX: Scheduling!Proportional-share (Scheduling)
   #+INDEX: Scheduling!Weighted round robin (Scheduling)
   #+INDEX: Scheduling!Weighted fair queuing (Scheduling)
   #+INDEX: Proportional-share scheduling (Scheduling)
   #+INDEX: Weighted round robin (WRR) (Scheduling)
   #+INDEX: Weighted fair queuing (WFQ) (Scheduling)
   - Simple form: *Weighted* [[#roundrobin][Round Robin]] (WRR)
     - Weight per thread is factor for length of time slice
     - Discussion
       - Con: Threads with high weight lead to long delays for others
       - Pro: Fewer context switches than following alternative
   #+ATTR_REVEAL: :frag appear
   - Alternative: *Weighted fair queuing* (WFQ)
     - Uniform time slice
     - Threads with lower weight “sit out” some iterations

*** WRR vs WFQ with sample Gantt Charts
    - Threads T1, T2, T3 with weights 3, 2, 1; q = 10ms
      - Supposed order of arrival: T1 first, T2 second, T3 third
      - If threads are not done after shown sequence, start over with T1

    {{{reveallicense("./figures/OS/4-wrr.meta","10vh",nil,t)}}}

    {{{reveallicense("./figures/OS/4-wfq.meta","10vh",nil,t)}}}

** CFS in Linux
   :PROPERTIES:
   :CUSTOM_ID: cfs
   :END:
   #+INDEX: Scheduling!Completely fair (Scheduling)
   #+INDEX: Completely fair scheduler (CFS) (Scheduling)
   - CFS = Completely fair scheduler
     - Actually, variant of [[#proportional-share][WRR]] above
       - Weights determined via so-called *niceness* values
         - (Lower niceness means higher priority)
   - *Core idea*
     #+ATTR_REVEAL: :frag (appear)
     - Keep track of how much threads were on CPU
       - Scaled according to weight
       - Called *virtual runtime*
         - Represented efficiently via [[https://en.wikipedia.org/wiki/Red%E2%80%93black_tree][red-black tree]]
     - Schedule thread that is furthest behind
       - Until preempted or time slice runs out
     - (Some details in in-class session and in cite:Hai17)

** Goals and Scheduling Mechanisms

   #+CAPTION: Figure 3.8 of cite:Hai17
   | Mechanism                                      | Goals                                 |
   |------------------------------------------------+---------------------------------------|
   | [[#fixed-priority][Fixed priority]]            | Urgency, importance                   |
   | [[#dynamic-priority][Earliest deadline first]] | Urgency                               |
   | [[#decay-usage][Decay usage]]                  | Importance, throughput, response time |
   | [[#proportional-share][Proportional share]]    | Resource allocation                   |

** JiTT Assignments
   :PROPERTIES:
   :reveal_data_state: jitt no-toc-progress
   :END:

    Submit your solutions to the following tasks in {{{jittquiz}}}.

*** Scheduling
    :PROPERTIES:
    :reveal_data_state: jitt no-toc-progress
    :END:

    Perform Round Robin scheduling given the following situation:

       | q=4 | Thread | Arrival Time | Service Time |
       |-----+--------+--------------+--------------|
       |     | T1     |            0 |            3 |
       |     | T2     |            1 |            6 |
       |     | T3     |            4 |            3 |
       |     | T4     |            9 |            6 |
       |     | T5     |           10 |            2 |

*** Ticket to Exam - Task 2
    :PROPERTIES:
    :CUSTOM_ID: ticket-3-2
    :reveal_data_state: jitt no-toc-progress
    :END:

    Perform Round Robin scheduling given the following situation:

       | q=3 | Thread | Arrival Time | Service Time |
       |-----+--------+--------------+--------------|
       |     | T1     |            0 |            2 |
       |     | T2     |            1 |            5 |
       |     | T3     |            9 |            4 |
       |     | T4     |           10 |            4 |

*** Battle of the Threads
    Prepare the in-class session by reading the
    [[./texts/instructions-file-server.pdf][instructions]] for
    the game Battle of the Threads.

*** Questions, Feedback, and Improvements
    :PROPERTIES:
    :reveal_data_state: jitt no-toc-progress
    :END:

    - {{{understandingquestion}}}


* In-Class Meeting
** Comments on Fixed-Priority Scheduling
   :PROPERTIES:
   :CUSTOM_ID: priority-starvation
   :END:
   #+INDEX: Starvation (Scheduling)
   - *Starvation* of low-priority threads possible
     - Starvation = continued denial of resource
       - Here, low-priority threads do not receive resource CPU as
         long as threads with higher priority exist
     - Careful design necessary
       - E.g., for hard-real-time systems (such as cars, aircrafts,
         power plants)
       - (Beware of [[file:Operating-Systems-MX-Challenges.org::#priority-inversion][priority inversion]],
         a topic for a later presentation!)

** Fair Queuing
   :PROPERTIES:
   :CUSTOM_ID: fair-queuing
   :END:
   #+INDEX: Fair queuing (Scheduling)
   - *Fair queuing* is a general technique
     - Also used, e.g., in computer networking
   - Intuition
     - System is *fair* if at all times every party has
       progressed according to its share
       - This would require infinitesimally small steps
   - Reality
     - Approximate fairness via “small” discrete steps
     - E.g., [[#cfs][CFS]]

*** CFS with Blocking
    - Above description of [[#cfs][CFS]] assumes runnable threads
    - Blocked threads lag behind
      - If blocked briefly, allow to catch up
      - If blocked for a long time (above threshold), they would
        deprive all other threads from CPU once awake again
        - Hence, counter-measure necessary
          - Give up fairness
        - Forward virtual runtime to be slightly less than minimum of
          previously runnable threads
    - Effect similar to dynamic priority adjustments of decay usage
      schedulers

*** CFS with Groups
    - CFS allows to assign threads to (hierarchies of) groups
      - Scheduling then aims to treat groups fairly
    - For example
      - One group per user
        - Every user obtains same CPU time
      - User-defined groups
        - E.g., multimedia with twice as much CPU time as programming
          (music and video running smoothly despite compile jobs)

** Thread Properties in Java
   :PROPERTIES:
   :CUSTOM_ID: java-thread-properties
   :END:
   #+INDEX: Java thread properties (Scheduling)
   - [[https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/lang/Thread.html][Java spec]]
     - “Every thread has a priority. Threads with higher priority are executed in preference to threads with lower priority.”
     - May interpret as: Preemptive, priority-driven scheduling
   - Priorities via integer values
     - Higher number → more CPU time
     - Preemptive
       - Current thread has highest priority
       - Newly created thread with higher priority replaces old one on CPU
       - Most of the time
   - Time slices not guaranteed (implementation dependent)
     - Starvation possible

** Battle of the Threads
   - Let’s play a round of Battle of the Threads
     - [[./texts/instructions-file-server.pdf][Instructions]]
     - [[file:Operating-Systems-Introduction.org::#battlethreads][Last time]],
       threads shared a magically synchronized data structure
     - This time, players share a data structure that is copied to
       and from a central location

* Conclusions
** Summary
   - OS performs scheduling for shared resources
     - Focus here: CPU scheduling
     - Subject to conflicting goals
   - CPU scheduling based on thread states and priorities
     - Fixed vs dynamic priorities vs proportional share
     - CFS as example for proportional share scheduling

#+MACRO: copyrightyears 2017, 2018, 2019
#+INCLUDE: "~/.emacs.d/oer-reveal-org/backmatter.org"
