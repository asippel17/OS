;;; publish.el --- Publish reveal.js presentation from Org files
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2017-2019 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Code:
(package-initialize)

;; For self-contained presentations that can be used offline, the file
;; index.css needs to be available.  That file is available at
;; oer.gitlab.io.  To embed here, different alternatives could be
;; used.  Embedding in every HTML head causes larger HTML files with
;; redundant contents.  Thus, download once and publish.
;; Link from Org file if necessary.
(require 'url)
(unless (file-exists-p "index.css") ; Avoid download in case of local copy.
  (url-copy-file "https://oer.gitlab.io/index.css" "index.css"))

;; Temporarily add oer-reveal and emacs-reveal as submodules for
;; faster development.  This is not necessary in the docker container,
;; which contains both.  However, it simplifies local maintenance and
;; experiments.
(add-to-list 'load-path
	     (expand-file-name "../oer-reveal/" (file-name-directory load-file-name)))
(add-to-list 'load-path
	     (expand-file-name "../emacs-reveal/" (file-name-directory load-file-name)))
(setq oer-reveal-publish-makeindex t)
(require 'oer-reveal)
(require 'emacs-reveal)

;; Publish reveal.js presentations.
(oer-reveal-publish-all
 (list
  (list "texts"
       	:base-directory "texts"
       	:base-extension "org"
       	:publishing-function '(org-latex-publish-to-pdf)
       	:publishing-directory "./public/texts")
  (list "orgs"
       	:base-directory "org-resources"
       	:base-extension "org"
       	:publishing-function 'org-publish-attachment
       	:publishing-directory "./public/org-resources")
  (list "index-terms"
	:base-directory "."
	;; A file theindex.org (which includes theindex.inc)
	;; is created due to assignment to
	;; oer-reveal-publish-makeindex above.
	;; Using that setting, the file is automatically published with
	;; org-re-reveal, which is useless.
	;; Thus, publish as HTML here.  For this to work, index-terms.org
	;; is a manually created file including theindex.inc.
	;; The preparation and completion functions below set up an
	;; advice on org-html-link to rewrite links into presentations
	;; using org-re-reveal's anchor format.
	:include '("index-terms.org")
	:exclude ".*"
	:preparation-function 'oer-reveal--add-advice-link
	:completion-function 'oer-reveal--remove-advice-link
	:publishing-function '(org-html-publish-to-html)
	:publishing-directory "./public")
  (list "images"
	:base-directory "img"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/img"
	:publishing-function 'org-publish-attachment
	:recursive t)
  (list "title-logos"
	:base-directory "non-free-logos/title-slide"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/title-slide"
	:publishing-function 'org-publish-attachment)
  (list "theme-logos"
	:base-directory "non-free-logos/reveal-css"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/reveal.js/css/theme"
	:publishing-function 'org-publish-attachment)
  (list "eval"
	:base-directory "eval"
	:base-extension (regexp-opt '("ods" "csv"))
	:publishing-directory "./public/eval"
	:publishing-function 'org-publish-attachment)
  (list "redirect"
	:base-directory "redirect"
	:base-extension 'any
	:publishing-directory "./public"
	:publishing-function 'org-publish-attachment)
  ))

(provide 'publish)
;;; publish.el ends here
